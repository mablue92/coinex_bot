import asyncio
import datetime
import json
import os
import re
import time
import urllib.request

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

list_url = "https://api.coinex.com/v1/market/list"
market_url = "https://api.coinex.com/v1/market/ticker?market="
bot = Bot(token=os.environ['TOKEN'])
# bot = Bot(token='')
loop = asyncio.get_event_loop()
bh = Dispatcher(bot=bot, loop=loop)


@bh.message_handler(commands=['start'])
async def say_hello(message: types.Message):
    await bot.send_message(message.chat.id, '` d e v e l o p i n g `', parse_mode='Markdown')


@bh.message_handler(commands=['ping'])
async def send_top(message: types.Message):
    s = time.time()
    msg = await message.reply('ping')
    await msg.edit_text(f'{(time.time() - s).2f}')


@bh.inline_handler()
async def inline_echo(inline_query: types.InlineQuery):  # query parts
    statement = re.search('/', string=inline_query.query)
    if statement:
        spl = str(inline_query.query).split('/')
        if len(spl) == 2:
            if spl[0].isalpha() and spl[1].isalpha():
                search = f"{spl[0].upper()} / {spl[1].upper()}" or 'BTC'
                response = urllib.request.urlopen(list_url)
                data = json.loads(response.read())
                k = 0
                items = []
                date_now = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
                for trade in data["data"]:
                    trade2 = trade
                    trade += "£"
                    trade = trade.replace('BTC£', ' / BTC')
                    trade = trade.replace('BCH£', ' / BCH')
                    trade = trade.replace('ETH£', ' / ETH')
                    trade = trade.replace('USDT£', ' / USDT')

                    if search not in trade:
                        continue

                    if k > 15:
                        continue

                    response_p = urllib.request.urlopen(market_url + trade2)
                    data_p = json.loads(response_p.read())
                    price = data_p["data"]["ticker"]
                    message = f'''
📢 *{trade}*
    
📤 *sell price:*  `{price["sell"]}`
📥 *buy price:*  `{price["buy"]}`
    
📉 *low price:*  `{price["low"]}`
📈 *high price:*  `{price["high"]}`
    
📦 *open price:*  `{price["open"]}`
🕘 *last price:*  `{price["last"]}`
#{spl[0].upper()}{spl[1].upper()}

Technical:
  Datetime utc: `{date_now}`
'''

                    input_content = types.InputTextMessageContent(message, parse_mode='Markdown')
                    but = types.InlineKeyboardButton(text='SHARE', switch_inline_query=f"{trade.replace(' / ','/')}")
                    markup = types.InlineKeyboardMarkup(row_width=1).row(but)
                    item = types.InlineQueryResultArticle(id=k, title=f"{trade}",
                                                          input_message_content=input_content,
                                                          reply_markup=markup)
                    items.append(item)
                    k += 1
                await bot.answer_inline_query(inline_query.id, results=items, cache_time=0)
    else:
        if str(inline_query.query).lower() == 'top':
            date_now = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
            text = "*TOP 10*\n"
            pairs = ["BTCBCH", "ETHBCH", "EOSBCH", "CETBCH", "CDYBCH", "CARDBCH", "HYDROBCH", "OLTBCH",
                     "LFTBCH", "NASHBCH"]
            key, items = int(), []
            for pair in pairs:
                response_p = urllib.request.urlopen(market_url + pair)
                data_p = json.loads(response_p.read())
                price = data_p["data"]["ticker"]
                key += 1
                text += f'''
*{pair}:*
📤  `{price["sell"]}` BCH
📥  `{price["buy"]}` BCH
                '''
            text += f"\nDate: {date_now}"
            input_content = types.InputTextMessageContent(text, parse_mode='Markdown')
            but = types.InlineKeyboardButton(text='SHARE TOP', switch_inline_query=f"TOP")
            markup = types.InlineKeyboardMarkup(row_width=1).row(but)
            item = types.InlineQueryResultArticle(id=key, title=f"TOP",
                                                  input_message_content=input_content,
                                                  reply_markup=markup)
            items.append(item)
            await bot.answer_inline_query(inline_query.id, results=items, cache_time=0)
        else:
            ...


if __name__ == '__main__':
    executor.start_polling(bh, loop=loop, skip_updates=True)
